const fetch = require("node-fetch");
const Discord = require('discord.js');
const cron = require('node-cron');
var os = require('os');
const client = new Discord.Client();
//config stuff
const config = require("./config.json");
const allowedSearch = ["rising", "hot", "top", "controversial", "new"];
const defaultSub = config.defaultSub;
const allSubs = config.subs;
//system stuff
var lastUsed = [];
var imgNum = 1;

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({
        status: "online",
        activity: {
            name: config.playing,
            type: "PLAYING"
        }
    });
});

client.on("message", async (msg) => {

    var args = msg.content.split(' ');
    console.log(args);

    if (args[0] === config.prefix) {

        if (msg.author.bot) return;

        //search random category
        if (args.length == 1) {
            var ran = Math.floor(Math.random() * allowedSearch.length);
            await msg.reply(await genMsg(defaultSub, allowedSearch[ran]));
            return;
        }

        //get specified search category from random sub
        if (args[1] === "rndsub") {

            for (var i = 0; i < allowedSearch.length; i++) {
                if (args[2] == allowedSearch[i]) {
                    var ran = Math.floor(Math.random() * allSubs.length);
                    await msg.reply(await genMsg(allSubs[ran], args[2]));
                    return;
                }
            }
            //respond error incluiding correct syntax
            await msg.reply("Invalid syntax! (" + config.prefix + " rndsub [search profile])");
        }

        //get specified search category from specified sub
        if (args[1] === "sub" && (parseInt(args[2]) <= allSubs.length - 1)) {

            for (var i = 0; i < allowedSearch.length; i++) {
                if (args[3] == allowedSearch[i]) {
                    await msg.reply(await genMsg(allSubs[parseInt(args[2])], args[3]));
                    return;
                }
            }
            //respond error incluiding correct syntax
            await msg.reply("Invalid syntax! (" + config.prefix + " sub [sub-id] [search profile])");
        }

        if (args[1] === "stats") {

            let botManagerRole = msg.guild.roles.cache.find(role => role.name === config.botManagerRole);

            if (msg.member.roles.cache.has(botManagerRole.id)) {
                await msg.reply(`\nNode version: ${process.version}\nPlatform: ${os.platform()}\nRam: ${os.totalmem() - os.freemem()}/${os.totalmem}\nCpu: ${os.cpus()[0].model}\nCurrent lstUsed: ${lastUsed.length}\nLatency: ${Date.now() - msg.createdTimestamp}ms`);
                return;
            }
            await msg.reply(`ERROR: Insufficient permissions!`);
            return;
        }

        //get and search specified category
        if (args.length == 2) {

            for (var i = 0; i < allowedSearch.length; i++) {
                if (args[1] == allowedSearch[i]) {
                    await msg.reply(await genMsg(defaultSub, args[1]));
                    return;
                }
            }
            //respond error incluiding correct syntax
            await msg.reply("Invalid syntax! (!" + config.prefix + " [search profile])");
        }

        await msg.reply("Invalid syntax!");
    }

})

//post newest from random sub every 10 min
cron.schedule(config.cron, async () => {

    //get ID of regular update chanel
    let channel = client.channels.cache.find(channel => channel.name.toLowerCase() === config.channelName);

    var ran = Math.floor(Math.random() * allSubs.length);
    channel.send(await genMsg(allSubs[ran], 'new'));
});

async function genMsg(sub, mode) {
    var obj = [];

    //reset last used if to large
    if (imgNum == 25) { lastUsed = []; imgNum = 0}

    console.log(lastUsed)

    while (true) {

        //fetch image urls from reddit
        console.log(sub, mode)
        var postUrl = `https://www.reddit.com/r/${sub}/${mode}.json?sort=${mode}`;
        await fetch(postUrl)
            .then(res => res.json())
            .then(json => obj = json);

        //check if link is duplicate
        for (var i = 0; i < lastUsed.length + 1; i++) {

            try {
                var url = obj.data.children[imgNum].data.url;
            } catch (error) {
                return "ERROR: Not enough posts present!";
            }

            if (url == lastUsed[i]) {
                imgNum++;
            } else if (i == lastUsed.length) {

                //check if image is in an embeddable format
                if (url.endsWith(".png") || url.endsWith(".jpg") || url.endsWith(".jpeg")) {

                    //generate and return embedd
                    const emb = new Discord.MessageEmbed()
                        .setColor('#0099ff')
                        .setFooter(obj.data.children[imgNum].data.title)
                        .setImage(url)
                        .setTitle(`Here, some ${mode} ${sub} for you`);

                    console.log(lastUsed.length, url)
                    console.log(lastUsed)
                    await lastUsed.push(url)
                    return emb;
                }

                //generate message with raw wurl
                lastUsed.push(url);
                return `Here, some ${mode} ${sub} for you \n ${url}`;
            }
        }
    }

}

client.login(config.token);